#!/bin/bash
#Name: LEDCalculator v0.1
#Author: Sam Cernik
#Created: 20/01/22

# This program is designed to assist in the calculations that are required to be carried out when evaluating different aspects of quoting for LED Screen jobs.
# The different featuires available in this program are:
# Panel Calculator: Calculates number of panels required based on a square metre area and panel height
# Ports Calculator: Calculates total pixels in a screen and number of ports/processors required
# Weight Calculator: Calculates the total weight of a screen's panels and rigging. 
# Features are still being designed and implemented.


#TO DO:
#If X is panel size and y is perpendicular length of screen what is screen angle?
#Aspect Ratio Calculator
#What is total resolution of screen using X number of Y panels

username=Victoria

#This function displays the main menu, allowing the user to navigate through the script.
function main_menu {
    clear
    echo "Welcome to LEDCalculator by Sam Cernik.
    Hello $username!
    What function would you like to use?: 
        [1] Panels Calculator
        [2] Ports Calculator
        [3] Screen Weight Caculator
        [4] Help
        [5] Exit Program"
        
    read -p "Your input: " mainMenuChoice

    #If statement that powers the menu
    if [[ $mainMenuChoice -eq 1 ]]
        then
            panel_calc
    elif [[ $mainMenuChoice -eq 2 ]]
        then
            echo "This has not been implemented yet. Please contact your administrator."
            ports_calc    
    elif [[ $mainMenuChoice -eq 3 ]]
        then
            weight_calc
    elif [[ $mainMenuChoice -eq 4 ]]
        then
            help_func
    elif [[ $mainMenuChoice -eq 5 ]]
        then
            echo "
    Thank you for using LEDCalculator.
    See you soon, $username!
    "
            sleep 1.5
            exit 1
    #If the exit option does not close your terminal window, you need to amend your terminal profile to allow exiting safely
    else
        read -p "This was not a valid input. Please press Enter to continue and try again."
        main_menu
    fi
}
#This function is designed to take a square metre area and panel height. 
#This then square roots the area, divides by the panel height, rounds this and then squares it - giving you a total number of panels.
#Eventually this function will also incorporate the option to enter a height and width of the screen, instead of just the area.
function panel_calc {
    clear
    echo "Panel Calculator"
    echo
    read -p "Please enter your total Screen Area (m^2): " screenArea
    read -p "Please enter your panel height(mm): " mmPanelHeight
    squareRoot=`echo "scale=4; sqrt($screenArea)" | bc`             #Square rooting the area
    temp=`echo "scale=3; $mmPanelHeight/1000" | bc`                 #As the panel height is taken in mm, but the area rooted into m - we must divide the panel height by 1000.
    printf -v panelHeight  "%.2f\n" "$temp"                         #This is formatting the floating point to the correct number of decimal places.
    rawSqrtPanels=`echo "scale=4; $squareRoot/$panelHeight" | bc`   #This gives you the unrounded square root of the total number of panels
    printf -v sqrtPanels "%.0f\n" "$rawSqrtPanels"                  #This line rounds the total number of panels up and records it in a new variable
    totalPanels=`echo "var=$sqrtPanels;var^=2;var" | bc`            #Finally, we square the rounded root to give us our final answer.
    echo "Your total number of panels is: $totalPanels"
    read -p "Press Enter to go back to the main menu..."
    main_menu

}

#This function is used by more than one feature, and allows an easy way of designating the particular panels 
#that are being used in the calculation and stores all the relevant information about them in variables.
function type_of_screen {
    echo "What type of panel are you using?
    
    [1] B02
    [2] B03
    [3] DM2
    [4] MC5
    [5] MC7
    "
    read -p "Your input: " panelChoice
                                            #To add a new panel, all that must be done is add a new elif statement before the fi in 
                                            #the same format as the below with the new information, and then simply add the name to the list in the above echo statement.
    if [[ $panelChoice -eq 1 ]]
    then
        pixelPitch=2.84
        panelPixels=30975
        panelType="B02"
        processorType="2"
        panelWeight=9.4
    elif [[ $panelChoice -eq 2 ]]
    then
        pixelPitch=3.47
        panelPixels=20736
        panelType="B03"
        processorType=2
        panelWeight=8.9
    elif [[ $panelChoice -eq 3 ]]
    then
        pixelPitch=2.6
        panelPixels=36864
        panelType="DM2"
        processorType=2
        panelWeight=5.76
    elif [[ $panelChoice -eq 4 ]]
    then
        pixelPitch=5.769
        panelPixels=10816
        panelType="MC5"
        processorType=1
        panelWeight=17
    elif [[ $panelChoice -eq 5 ]]
    then
        pixelPitch=7.5
        panelPixels=6400
        panelType="MC7"
        processorType=1
        panelWeight=15.7
    fi
}

function m2 {
    portOutput=525000
    numOfPorts=4
    proName="Tessera M2"
}

function s4 {
    portOutput=525000
    numOfPorts=4
    proName="Tessera S4"
}

function s8 {
    portOutput=525000
    numOfPorts=8
    proName="Tessera S8"
}

function sx40 {
    portOutput=9000000
    numOfPorts=4
    proName="Tessera SX40"
}
 
function ev4 {
    portOutput=655360
    numOfPorts=10
    proName="EV4 Processor"
}

function hd102 {
    portOutput=655000
    numOfPorts=4
    proName="HD102 Processor"
}
function hd101 {
    portOutput=655000
    numOfPorts=4
    proName="HD101 Processor"
}

function processorSummary {
    echo "
    Processor Name: $proName
    Output per port: $portOutput px
    Number of ports: $numOfPorts"
    pixelsPorts=`echo "$totalPixels/$portOutput" | bc`
    
    if [[ pixelsPorts -lt 1 ]]
    then
        totalPorts=1
    elif [[ pixelsPorts -eq 0 ]]
    then
        totalPorts=1
    else   
        totalPorts=`echo "$pixelsPorts+1" | bc`
    fi
    numOfProcs=`echo "($totalPorts/$numOfPorts)+1" | bc`
    echo "  Number of ports required for job: $totalPorts"
    echo "  Number of Processors required: $numOfProcs"
}

function processors {
    if [[ $processorType -eq 1 ]]
    then
        ev4
        processorSummary
        hd102
        processorSummary
        hd101
        processorSummary
    elif [[ $processorType -eq 2 ]]
    then
        m2
        processorSummary
        s4
        processorSummary
        s8
        processorSummary
        sx40
        processorSummary
     fi   
}

function ports_calc {
    clear
    echo "Port Calculator
    "
    type_of_screen
    read -p "Panel Count: " panelCount
    echo "
You have selected:
    Panel Type:     $panelType
    Pixel Pitch:    $pixelPitch
    Panel Pixels:   $panelPixels
    "
    totalPixels=`echo "$panelPixels*$panelCount" | bc`


    echo "Available Processors:"
    processors

    read -p "Press Enter to go back to the main menu..."
    main_menu
}

function weight_calc {
    clear
    echo "Weight Calculator
    
All weight is measured in KG
    "
    type_of_screen
    read -p "Panel Count: " panelCount
    echo "
You have selected:
    Panel Type:     $panelType
    Panel Weight:    $panelWeight"

    screenWeight=`echo "$panelWeight*$panelCount" | bc`
    riggingWeight=`echo "$screenWeight*0.1" | bc`
    totalScreenWeight=`echo "$screenWeight*1.1" | bc`

    echo "
    Total weight of panels: $screenWeight
    Total weight of rigging: $riggingWeight
    Total weight of screen: $totalScreenWeight"
    echo
    read -p "Press Enter to go back to the main menu..."
    main_menu
    
}

function help_func {



clear
echo "Help: 

Name: LEDCalculator v1.0
Author: Sam Cernik
Created: 20/01/22

This program is designed to assist in the calculations that are required to be carried out when evaluating different aspects of quoting for LED Screen jobs.
The different featuires available in this program are:
Panel Calculator: Calculates number of panels required based on a square metre area and panel height
Ports Calculator: Calculates total pixels in a screen and number of ports/processors required
Weight Calculator: Calculates the total weight of a screen's panels and rigging. 
Features are still being designed and implemented.
"

read -p "Press Enter to return to the main menu..."
main_menu

}

main_menu


# LED Processors
# Evision - Outdoor (Low Res) [processor type 1]
#     EV4 Processor
#         Port Outputs: 10 
#         Max output per port: 655,360 px @60Hz, 8 bit
#         Loading Capacity: 8.3 million pixels 
#         Uses Evision EVX10 Fibre Box 
#     HD102 Processor 
#         Port Outputs: 4 
#         Input: HDMI / DVI 
#         Maximum output per port: 655,000 px @ 60Hz
#     HD101 Processor 
#         Port Outputs: 4 
#         Input: HDMI / DVI 
#         Maximum output per port: 655,000 px @ 60Hz

# Brompton - Indoor (High Res)  [processor type 2]
#     Tessera M2 
#         Port Outputs: 4
#         Inputs: 1x DVI, 2x SD/HD/3G-SDI 
#         Maximumum outputs per port: 525000 px @ 60Hz, 8 bit
#     Tessera S4
#         Port Outputs: 4
#         Input: DVI-D
#         Maximumum outputs per port: 525,000 px @ 60Hz, 8 bit 
#     Tessera S8 
#         Port Outputs: 8
#         Inputs: SDI / HDMI
#         Maximumum outputs per port: 525,000 px @ 60Hz, 8 bit 
#         10 & 12 Bit supported (reduced pixel count) 
#     Tessera SX40 
#         Port Outputs: 4 (4 ethernet out, 4 fibre out)
#         Inputs: SDI / HDMI
#         Maximum outputs per port: 9 million pixels @ 60Hz, 36 bit
#         Connects to Bromton Tessera XD
#     
#     Brompton Tessera XD 
#         Inputs: 1x Ethernet / 1x Fibre 
#         Outputs: 10x Ethernet out 

# BO2 (2.84)
# Pixel / Tile - 176 x 176 = 30,975 px 
# Comes in cases of 8. 
# 9.4kg per panel.

# BO3 (3.47)
# Pixel / Tile - 144 x 144 = 20,736 px  
# Cases of 8
# 8.9kg per panel

# DM2 (2.6)
# Pixel / Tile - 192 x 192 = 36,864 px 
# Cases of 10
# 5.76kg per panel

# MC5 
# 17kg in weight - add 10% for cabling
# Pixel Pitch - 5.769mm 
# Pixel / Tile - 104 x 104 =  10,816 px

# MC7 
# 15.7kg in weight - add 10% for cabling
# Pixel Pitch - 7.5mm 
# Pixel / Tile - 80 x 80 = 6400 px