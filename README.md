## LEDCalculator
**By Sam Cernik**

You�ll start by editing this README file to learn how to edit a file in Bitbucket.

Updated: 07/02/22

This program is designed to assist in the calculations that are required to be carried out when evaluating different aspects of quoting for LED Screen jobs.
The different featuires available in this program are:

1. **Panel Calculator**: Calculates number of panels required based on a square metre area and panel height
2. **Ports Calculator**: Calculates total pixels in a screen and number of ports/processors required
3. **Weight Calculator**: Calculates the total weight of a screen's panels and rigging. 
4. **Resolution Calculator**: Takes either the width and height of a screen in panels, or total panels, and outputs the total resolution of the screen in  pixels. If you give width and height, the function also outputs the resolution in terms of w x h.
5. **Aspect Ratio Calculator**: Calculates Aspect Ratio of a screen using width and height. Works for resolution or physical dimensions.
6. **Resolution Calculator**: Takes either the width and height of a screen in panels, or total panels, and outputs the total resolution of the screen in  pixels. If you give width and height, the function also outputs the resolution in terms of w x h.
7. **Angle Calculator**: Takes in the diamter of a circle and the type of panel you are using. It outputs the appropiate angle between each panel to create a curve that fits the circle.
Features are still being designed and implemented.

---

## How to run
**Running from terminal**

1. If you are comfortable using the commandline, download [ledCalc.sh](https://bitbucket.org/samcernik/ledcalc/src/master/ledCalc.sh) by clicking the menu button, right clicking **Open Raw** and then clicking **Download Linked File as...** and saving the file as **ledCalc.sh**. 
2. Place this in any folder, but remember where it is.
3. Open terminal, and navigate to the folder you saved the script in using the cd command.
4. In this folder, execute the following command: "**chmod +rx ledCalc.sh**". This will set the permissions for executing the file, allowing you to run this. Without this, the terminal will return: "/bin/bash: ./ledCalc.sh: Permission denied"
5. Now you can run the program, execute the following command: "**./ledCalc.sh**"

**Running from desktop**

1. To make using this program easier, I have also made a .command file, which will allow you to double click to execute rather than having to find it in the terminal every time. Download [ledCalc.command](https://bitbucket.org/samcernik/ledcalc/src/master/ledCalc.command) by clicking the menu button, right clicking **Open Raw** and then clicking **Download Linked File as...** and saving the file as **ledCalc.command** wherever you want.
2. There is one pre-requisite to complete in the terminal before you use it however. Open terminal and navigate to the folder the script is located using the **cd** command. (Your terminal will open in your home folder, so if you saved the file in your Documents folder, the command would simply be "**cd Documents**").
3. Execute the command "**chmod +x ledCalc.command**"
4. Now you can run the program from the finder window by simply double clicking it.
