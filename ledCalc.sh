#!/bin/bash
#Name: LEDCalculator
#Author: Sam Cernik
#Updated: 08/02/22

# This program is designed to assist in the calculations that are required to be carried out when evaluating different aspects of quoting for LED Screen jobs.
# The different featuires available in this program are:
# Panel Calculator: Calculates number of panels required based on a square metre area and panel height. Also now outputs the total area if you given a number of panels.
# Ports Calculator: Calculates total pixels in a screen and number of ports/processors required
# Weight Calculator: Calculates the total weight of a screen's panels and rigging. 
# Aspect Ratio Calculator: Calculates the aspect ratio of a screen, given the screen width and height. Also works with resolution.
# Resolution Calculator: Takes either the width and height of a screen in panels, or total panels, and outputs the total resolution of the screen in  pixels. If you give width and height, the function also outputs the resolution in terms of w x h.
# Angle Calculator: Takes in the diamter of a circle and the type of panel you are using. It outputs the appropiate angle between each panel to create a curve that fits the circle. Also now outputs the diameter if an angle is given.
# Features are still being designed and implemented.




username=Victoria
version_number="V0.7"
#This function displays the main menu, allowing the user to navigate through the script.
function main_menu {
    clear
    echo "Welcome to LEDCalculator $version_number by Sam Cernik.
    Hello $username!
    What function would you like to use?: 
        [1] Panels Calculator
        [2] Ports Calculator
        [3] Screen Weight Caculator
        [4] Aspect Ratio Calculator
        [5] Resolution Calculator
        [6] Screen Curve Angle Calculator
        [7] Help
        [8] Exit Program"
        
    read -p "Your input: " mainMenuChoice

    #If statement that powers the menu
    if [[ $mainMenuChoice -eq 1 ]]
        then
            panel_calc
    elif [[ $mainMenuChoice -eq 2 ]]
        then
            ports_calc    
    elif [[ $mainMenuChoice -eq 3 ]]
        then
            weight_calc
    elif [[ $mainMenuChoice -eq 4 ]]
        then
            aspect_calc
    elif [[ $mainMenuChoice -eq 5 ]]
        then
            resolution_calc
     elif [[ $mainMenuChoice -eq 6 ]]
        then
            angle_calc
    elif [[ $mainMenuChoice -eq 7 ]]
        then
            help_func
    elif [[ $mainMenuChoice -eq 8 ]]
        then
            echo "
    Thank you for using LEDCalculator.
    See you soon, $username!
    "
            sleep 1.5
            exit 1
    #If the exit option does not close your terminal window, you need to amend your terminal profile to allow exiting safely
    else
        read -p "This was not a valid input. Please press Enter to continue and try again."
        main_menu
    fi
}
#This function is designed to take a square metre area and panel height. 
#This then square roots the area, divides by the panel height, rounds this and then squares it - giving you a total number of panels.
#Eventually this function will also incorporate the option to enter a height and width of the screen, instead of just the area.
function count_calc {
    echo
    type_of_screen
    read -p "Please enter your total Screen Area (m^2): " screenArea
    squareRoot=`echo "scale=4; sqrt($screenArea)" | bc`             #Square rooting the area
    temp=`echo "scale=3; $panelHeight/1000" | bc`                 #As the panel height is taken in mm, but the area rooted into m - we must divide the panel height by 1000.
    printf -v panelHeight  "%.2f\n" "$temp"                         #This is formatting the floating point to the correct number of decimal places.
    rawSqrtPanels=`echo "scale=4; $squareRoot/$panelHeight" | bc`   #This gives you the unrounded square root of the total number of panels
    printf -v sqrtPanels "%.0f\n" "$rawSqrtPanels"                  #This line rounds the total number of panels up and records it in a new variable
    totalPanels=`echo "var=$sqrtPanels;var^=2;var" | bc`            #Finally, we square the rounded root to give us our final answer.
    echo "Your total number of panels is: $totalPanels"
    read -p "Press Enter to go back to the main menu..."
    main_menu

}

function area_calc {
    echo
    type_of_screen
    read -p "Please enter screen width in panels: " screenWidth
    read -p "Please enter screen height in panels: " screenHeight
    mmScreenHeight=`echo "$screenHeight*$panelHeight" | bc`
    mmScreenWidth=`echo "$screenWidth*$panelHeight" | bc`
    mmScreenArea=`echo "$mmScreenWidth*$mmScreenHeight" | bc`
    screenArea=`echo "$mmScreenArea/1000000" | bc`
    totalPanels=`echo "$screenWidth*$screenHeight" | bc`
    echo "Total Panels: $totalPanels"
    echo "Total Screen area: $screenArea m^2"
    read -p "Press enter to return to main menu..."
    main_menu

}

function panel_calc {
    clear
    echo "Panel Calculator
    
    Choose an option:
    [1] Area
    [2] Panel Count"

    read -p "Your input: " panelChoice

    if [[ panelChoice -eq 1 ]]
        then
            area_calc
    elif [[ panelChoice -eq 2 ]] 
        then
            count_calc
    fi
}

#This function is used by more than one feature, and allows an easy way of designating the particular panels 
#that are being used in the calculation and stores all the relevant information about them in variables.
function type_of_screen {
    echo "What type of panel are you using?
    
    [1] B02
    [2] B03
    [3] DM2
    [4] MC5
    [5] MC7
    "
    read -p "Your input: " panelChoice
                                            #To add a new panel, all that must be done is add a new elif statement before the fi in 
                                            #the same format as the below with the new information, and then simply add the name to the list in the above echo statement.
    if [[ $panelChoice -eq 1 ]]
    then
        pixelPitch=2.84
        panelPixels=30975
        panelResHeight=176
        panelType="B02"
        processorType="2"
        panelWeight=9.4
        panelHeight=500
        panelHeightM=0.5
    elif [[ $panelChoice -eq 2 ]]
    then
        pixelPitch=3.47
        panelPixels=20736
        panelResHeight=144
        panelType="B03"
        processorType=2
        panelWeight=8.9
        panelHeight=500
        panelHeightM=0.5
    elif [[ $panelChoice -eq 3 ]]
    then
        pixelPitch=2.6
        panelPixels=36864
        panelResHeight=192
        panelType="DM2"
        processorType=2
        panelWeight=5.76
        panelHeight=500
        panelHeightM=0.5
    elif [[ $panelChoice -eq 4 ]]
    then
        pixelPitch=5.769
        panelPixels=10816
        panelResHeight=104
        panelType="MC5"
        processorType=1
        panelWeight=17
        panelHeight=600
        panelHeightM=0.6
    elif [[ $panelChoice -eq 5 ]]
    then
        pixelPitch=7.5
        panelPixels=6400
        panelResHeight=80
        panelType="MC7"
        processorType=1
        panelWeight=15.7
        panelHeight=600
        panelHeightM=0.6
    fi
}

function m2 {
    portOutput=525000
    numOfPorts=4
    proName="Tessera M2"
}

function s4 {
    portOutput=525000
    numOfPorts=4
    proName="Tessera S4"
}

function s8 {
    portOutput=525000
    numOfPorts=8
    proName="Tessera S8"
}

function sx40 {
    portOutput=9000000
    numOfPorts=4
    proName="Tessera SX40"
}
 
function ev4 {
    portOutput=655360
    numOfPorts=10
    proName="EV4 Processor"
}

function hd102 {
    portOutput=655000
    numOfPorts=4
    proName="HD102 Processor"
}
function hd101 {
    portOutput=655000
    numOfPorts=4
    proName="HD101 Processor"
}

function processorSummary {
    echo "
    Processor Name: $proName
    Output per port: $portOutput px
    Number of ports: $numOfPorts"
    pixelsPorts=`echo "$totalPixels/$portOutput" | bc`
    
    if [[ pixelsPorts -lt 1 ]]
    then
        totalPorts=1
    elif [[ pixelsPorts -eq 0 ]]
    then
        totalPorts=1
    else   
        totalPorts=`echo "$pixelsPorts+1" | bc`
    fi
    numOfProcs=`echo "($totalPorts/$numOfPorts)+1" | bc`
    echo "  Number of ports required for job: $totalPorts"
    echo "  Number of Processors required: $numOfProcs"
}

function processors {
    if [[ $processorType -eq 1 ]]
    then
        ev4
        processorSummary
        hd102
        processorSummary
        hd101
        processorSummary
    elif [[ $processorType -eq 2 ]]
    then
        m2
        processorSummary
        s4
        processorSummary
        s8
        processorSummary
        sx40
        processorSummary
     fi   
}

function ports_calc {
    clear
    echo "Port Calculator
    "
    type_of_screen
    read -p "Panel Count: " panelCount
    echo "
You have selected:
    Panel Type:     $panelType
    Pixel Pitch:    $pixelPitch
    Panel Pixels:   $panelPixels
    "
    totalPixels=`echo "$panelPixels*$panelCount" | bc`


    echo "Available Processors:"
    processors

    read -p "Press Enter to go back to the main menu..."
    main_menu
}

function weight_calc {
    clear
    echo "Weight Calculator
    
All weight is measured in KG
    "
    type_of_screen
    read -p "Panel Count: " panelCount
    echo "
You have selected:
    Panel Type:     $panelType
    Panel Weight:    $panelWeight"

    screenWeight=`echo "$panelWeight*$panelCount" | bc`
    riggingWeight=`echo "$screenWeight*0.1" | bc`
    totalScreenWeight=`echo "$screenWeight*1.1" | bc`

    echo "
    Total weight of panels: $screenWeight
    Total weight of rigging: $riggingWeight
    Total weight of screen: $totalScreenWeight"
    echo
    read -p "Press Enter to go back to the main menu..."
    main_menu
    
}

function aspect_calc {
    clear
    echo "Aspect Ratio Calculator    
     "
    read -p "Please enter your screen length: " screenLength
    read -p "Please enter your screen width: " screenWidth
    temp=`echo "scale=3; $screenLength/$screenWidth" | bc`
    printf -v rawRatio "%0.2f\n" "$temp" 
    sixnine=`echo "scale=3; 16/9" | bc`
    printf -v sixnine "%0.2f\n" "$sixnine"
    fourthree=`echo "scale=3; 4/3" | bc`
    printf -v fourthree "%0.2f\n" "$fourthree"
    sixten=`echo "scale=3; 16/10" | bc`
    printf -v sixten "%0.2f\n" "$sixten"
    aspectRatio="1:$rawRatio"
    if [[ $rawRatio == $sixnine ]]
        then
            aspectRatio="16:9"
    elif [[ $rawRatio = $fourthree ]]
        then
            aspectRatio="4:3"
    elif [[ $rawRatio = $sixten ]]
        then
            aspectRatio="16:10"
    fi
    echo "
    Aspect Ratio of Screen: $aspectRatio
    
    "
    read -p "Press Enter to go back to the main menu..."
    main_menu

}

function resolution_calc {
    clear
    echo  "Screen Resolution Calculator
    "
    echo "Choose from one of the following options:
    
    [1] Width and Height
    [2] Total Panels"

    read -p "Your input: " resChoice

    if [[ resChoice -eq 1 ]]
    then
        wah
    elif [[ resChoice -eq 2 ]]
    then
        tot
    else
        read -p "This was not a valid input. Please press enter to try again..."
    fi

    read -p "Press Enter to go back to the main menu..."

    main_menu



}

function wah {
    type_of_screen
    read -p "Please enter the Width of the screen in panels: " screenWidth
    read -p "Please enter the Height of the screen in panels: " screenHeight

    wideResolution=`echo "$screenWidth*$panelResHeight" | bc`
    highResolution=`echo "$screenHeight*$panelResHeight" | bc`
    totalResolution=`echo "$wideResolution*$highResolution" | bc`

    echo "Screen Resolution = $wideResolution x $highResolution
    Total Pixels = $totalResolution"

}

function tot {
    type_of_screen
    read -p "Please enter the total number of panels: " screenPanels

    totalResolution=`echo "$screenPanels*$panelPixels" | bc`

    echo "Total Pixels = $totalResolution"

}

function angle_calc {
    clear
    echo "Screen Angle Calculator
    
    Please choose on of the following options:
    [1] Angle Calculator
    [2] Diameter Calculator
    " 
    read -p "Your input: " angle_choice
    if [[ $angle_choice -eq 1 ]]
        then
            angle_main
    elif [[ $angle_choice -eq 2 ]]
        then
            diam_calc
    else
        read -p "$angle_choice was not a valid input. Please press enter and try again..."
        angle_calc
    fi
}

function angle_main {
    clear
    echo "Screen Angle Calculator
    "
    type_of_screen
    read -p "Enter circle diameter (m): " circleDiam

    # Formular for circles 
	# 360 (Circle degrees) / degree change (e.g 6 degree curve) x panel size (50cm) x Pi (3.1415) = Circle Diameter 
    # 360x50xpi/diam = angle
    circleDiam=`echo "$circleDiam*1000" | bc`
    #diameter multipled by 1000 to turn it into mm.
    screenAngle=`echo "(360*$panelHeight*(4*a(1)))/$circleDiam" | bc -l`
    echo "Screen curve angle: $screenAngle
    "
    read -p "Press Enter to return to the main menu..."
    main_menu

}

function diam_calc {
    clear
    echo "Diameter Calculator
    "
    type_of_screen
    read -p "Enter Screen Curve angle (deg): " angle

    # Formular for circles 
	# 360 (Circle degrees) / degree change (e.g 6 degree curve) x panel size (50cm) x Pi (3.1415) = Circle Diameter 
    # 360x50xpi/diam = angle
    
    circleDiam=`echo "scale=3; (360*$panelHeightM*(4*a(1)))/$angle" | bc -l`
    # temp=`echo "$mmCircleDiam/1000" | bc`
    # printf -v circleDiam "%.3f\n" "$temp"   
    echo "Circle Diameter: $circleDiam m"
    read -p "Press Enter to return to the main menu..."
    main_menu
}

function help_func {
    clear
    echo "Help: 

    Name: LEDCalculator $version_number
    Author: Sam Cernik
    Updated: 08/02/22

    This program is designed to assist in the calculations that are required to be carried out when evaluating different aspects of quoting for LED Screen jobs.
    The different featuires available in this program are:
    Panel Calculator: Calculates number of panels required based on a square metre area and panel height. Also now outputs the total area if you given a number of panels.
    Ports Calculator: Calculates total pixels in a screen and number of ports/processors required
    Weight Calculator: Calculates the total weight of a screen's panels and rigging. 
    Aspect Ratio Calculator: Calculates Aspect Ratio of a screen using width and height. Works for resolution or physical dimensions.
    Resolution Calculator: Takes either the width and height of a screen in panels, or total panels, and outputs the total resolution of the screen in  pixels. If you give width and height, the function also outputs the resolution in terms of w x h.
    Angle Calculator: Takes in the diamter of a circle and the type of panel you are using. It outputs the appropiate angle between each panel to create a curve that fits the circle. Also now outputs the diameter if an angle is given.
    Features are still being designed and implemented.
    "

    read -p "Press Enter to return to the main menu..."
    main_menu
}

main_menu


# LED Processors
# Evision - Outdoor (Low Res) [processor type 1]
#     EV4 Processor
#         Port Outputs: 10 
#         Max output per port: 655,360 px @60Hz, 8 bit
#         Loading Capacity: 8.3 million pixels 
#         Uses Evision EVX10 Fibre Box 
#     HD102 Processor 
#         Port Outputs: 4 
#         Input: HDMI / DVI 
#         Maximum output per port: 655,000 px @ 60Hz
#     HD101 Processor 
#         Port Outputs: 4 
#         Input: HDMI / DVI 
#         Maximum output per port: 655,000 px @ 60Hz

# Brompton - Indoor (High Res)  [processor type 2]
#     Tessera M2 
#         Port Outputs: 4
#         Inputs: 1x DVI, 2x SD/HD/3G-SDI 
#         Maximumum outputs per port: 525000 px @ 60Hz, 8 bit
#     Tessera S4
#         Port Outputs: 4
#         Input: DVI-D
#         Maximumum outputs per port: 525,000 px @ 60Hz, 8 bit 
#     Tessera S8 
#         Port Outputs: 8
#         Inputs: SDI / HDMI
#         Maximumum outputs per port: 525,000 px @ 60Hz, 8 bit 
#         10 & 12 Bit supported (reduced pixel count) 
#     Tessera SX40 
#         Port Outputs: 4 (4 ethernet out, 4 fibre out)
#         Inputs: SDI / HDMI
#         Maximum outputs per port: 9 million pixels @ 60Hz, 36 bit
#         Connects to Bromton Tessera XD
#     
#     Brompton Tessera XD 
#         Inputs: 1x Ethernet / 1x Fibre 
#         Outputs: 10x Ethernet out 

# BO2 (2.84)
# Pixel / Tile - 176 x 176 = 30,976 px 
# Comes in cases of 8. 
# 9.4kg per panel.

# BO3 (3.47)
# Pixel / Tile - 144 x 144 = 20,736 px  
# Cases of 8
# 8.9kg per panel

# DM2 (2.6)
# Pixel / Tile - 192 x 192 = 36,864 px 
# Cases of 10
# 5.76kg per panel

# MC5 
# 17kg in weight - add 10% for cabling
# Pixel Pitch - 5.769mm 
# Pixel / Tile - 104 x 104 =  10,816 px

# MC7 
# 15.7kg in weight - add 10% for cabling
# Pixel Pitch - 7.5mm 
# Pixel / Tile - 80 x 80 = 6400 px